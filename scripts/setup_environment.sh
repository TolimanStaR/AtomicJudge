#!/bin/bash


apt update
apt upgrade


gcc -v

# shellcheck disable=SC2034
res=$?

if [ -$res -ne "0" ]; then
  apt install gcc -y
fi

g++ -v

# shellcheck disable=SC2034
res=$?

if [ -$res -ne "0" ]; then
  apt install g++ -y
fi

python2 --version

# shellcheck disable=SC2034
res=$?

if [ -$res -ne "0" ]; then
  apt install python2 -y
fi

python3 --version

# shellcheck disable=SC2034
res=$?

if [ -$res -ne "0" ]; then
  apt install python3 -y;
  apt install python3-pip libpq-dev;
  pip3 install psycopg2-binary django
fi

javac -version

# shellcheck disable=SC2034
res=$?

if [ -$res -ne "0" ]; then
  apt install default-jre -y;
  apt install default-jdk -y
fi


python3 main.py