from itertools import combinations as c
n, k=map(int, input().split())
a = list(map(int, input().split()))
best_n = 0
for sol in c(list(range(n)), k):
	res = 0
	sol=list(sol)
	s=''
	c=sol[0]
	ar=a[c + 1:]+a[:c + 1]
	for i in range(k):
		sol[i]=(sol[i]+n-(c + 1))%n
	sol=sol[1:]
	t=[0]*n
	for x in sol:
		t[x]=1
	for i in range(n):
		si = '|' if t[i] else '&'
		s+=f'{ar[i]}{si}'
	e = eval(s[:-1])
	if e > best_n:
		best_n = e

print(best_n)
